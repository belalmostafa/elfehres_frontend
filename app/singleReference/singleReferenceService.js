angular.module('elfehres.reference')
    .service('getReference', function ($q, $http, base_url, elfehres_httpSerivce){

        //TODO finish the back end call
        var getReferenceWithId = function(params){

            var deferred = $q.defer();

            elfehres_httpSerivce.get('question/'+params,{}, false).then(function (result){
                deferred.resolve(result);
            },function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return {
            getReferenceWithId : getReferenceWithId,
        }
    });
