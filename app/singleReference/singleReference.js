'use strict';

var singleQuestion  =   angular.module('elfehres.reference', ['ui.router', 'ui.bootstrap']);

singleQuestion.config(function($stateProvider) {

    $stateProvider.state('reference', {
        abstract: true,
        url : '/references/{referenceId}',
        templateUrl : "singleReference/index.html",
        controller : "singleReferenceDisplayController"
    });
});

singleQuestion.controller('singleReferenceDisplayController', singleReferenceDisplayFunction);

singleReferenceDisplayFunction.$inject=['$scope', '$state', '$uibModal','$stateParams','getReference'];

//helper function
function groupRepliesByReplyType(jsonObject){
    var newObject   =   [];
            jsonObject.forEach(function(currentObjectOfJsonObject){
                if(newObject[currentObjectOfJsonObject.reply.type]){
                   newObject[currentObjectOfJsonObject.reply.type].push(currentObjectOfJsonObject);
                }else{
                    newObject[currentObjectOfJsonObject.reply.type]   =   [];
                    newObject[currentObjectOfJsonObject.reply.type].push(currentObjectOfJsonObject);
                }
            });
    return newObject;
}

function quillGetHTML(inputDelta) {
    var tempCont = document.createElement("div");
    tempCont.style.display  =   'none';
    document.body.appendChild(tempCont);
    (new Quill(tempCont)).setContents(JSON.parse(inputDelta));
    return  tempCont.getElementsByClassName("ql-editor")[0].innerHTML;
}

function singleReferenceDisplayFunction($scope, $state, $uibModal, $stateParams, getReference){
    $scope.object = {};
    $scope.$state = $state;

    getReference.getReferenceWithId($stateParams.referenceId).then(function (question){
        $scope.object.question  = question;
        document.getElementById('questionNotes').innerHTML  =   quillGetHTML(question.questionNotes);
       $scope.object.question.replies =   groupRepliesByReplyType($scope.object.question.replies);
    },function(error) {
    });

}
