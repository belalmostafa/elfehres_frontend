'use strict';

angular.module('elfehres')
    .component('requestReferencesModalComponent', {
        templateUrl: '/requestReferencesComponent/index.html',
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        },

        controller: function e(requestReferencesService, $cookies, $state) {
            var $ctrl = this;
            $ctrl.tags=[];


            $ctrl.quill = new Quill('#editor-questionDetails', {
                modules: {
                    toolbar: true
                },
                scrollingContainer: '#scrolling-container',
                placeholder: 'Compose an epic...',
                theme: 'snow'
            });

            $ctrl.cancel = function () {
                $ctrl.dismiss({$value: 'cancel'});
            };

            $ctrl.askForReferences      =   function(){
                console.log($cookies.get('_tk'));
                var questionTitle       =   document.getElementById('questionTitle').value;
                var questionDescription =   JSON.stringify($ctrl.quill.getContents());
                var tags                =   JSON.stringify($ctrl.tags);
                console.log(tags);
                var questionRequest     =   {
                    'token'         :   $cookies.get('_tk'),
                    'title'         :   questionTitle,
                    'notes'   :   questionDescription,
                    'tags'          :   tags
                };




                requestReferencesService.askForReferences(questionRequest).then(function (response){
                    $ctrl.cancel();
                       $state.go('reference.books', {referenceId: response.id });
                    },function(error) {
                        console.log("fail");
                    });
            };
        }
    });