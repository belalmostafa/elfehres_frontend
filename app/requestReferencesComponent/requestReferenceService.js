angular.module('elfehres')
    .service('requestReferencesService', function ($q, $http, base_url, elfehres_httpSerivce){

        //TODO finish the back end call
        var askForReferences = function(params){

            var deferred = $q.defer();

            elfehres_httpSerivce.post('question', params).then(function (result){
                deferred.resolve(result);
            },function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return {
            askForReferences : askForReferences
        }
    });
