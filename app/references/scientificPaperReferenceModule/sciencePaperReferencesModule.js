'use strict';

var scientificPaper =   angular.module('elfehres.reference.scp', ['ui.router', 'ui.bootstrap']);

scientificPaper.config(function($stateProvider) {

    $stateProvider.state('reference.paper', {
        url : '/references/{referenceId}/paper',
        templateUrl : "references/scientificPaperReferenceModule/sciencePaperReferencesView.html",
        controller : "sciencePaperReferencesController"
    });
});

scientificPaper.controller('sciencePaperReferencesController', singleReferenceDisplayFunction);

singleReferenceDisplayFunction.$inject=['$scope', '$state', '$uibModal','$stateParams'];

function singleReferenceDisplayFunction($scope, $state, $uibModal, $stateParams){
    console.log($scope.object);
    // $scope.object = {};
    // $scope.$state = $state;
    // getBooks.getBooksOfQuestionId({'questionId': $stateParams.referenceId}).then(function (replies){
    //     $scope.object.books  = replies;
    //     console.log("hello");
    //     console.log($scope.object.books);
    // },function(error) {
    // });
}
