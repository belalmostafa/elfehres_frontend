'use strict';

var book    =   angular.module('elfehres.reference.books', ['ui.router', 'ui.bootstrap']);

book.config(function($stateProvider) {

    $stateProvider.state('reference.books', {
        url : '/references/{referenceId}/books',
        templateUrl : "references/bookReferencesComponent/bookReferencesView.html",
        controller : "bookReferencesController"
    });
});

book.controller('bookReferencesController', singleReferenceDisplayFunction);

singleReferenceDisplayFunction.$inject=['$scope', '$state', '$uibModal','$stateParams','getBooks'];

function singleReferenceDisplayFunction($scope, $state, $uibModal, $stateParams, getBooks){
    // $scope.object = {};
    // $scope.$state = $state;
    // getBooks.getBooksOfQuestionId({'questionId': $stateParams.referenceId}).then(function (replies){
    //     $scope.object.books  = replies;
    //     console.log("hello");
    //     console.log($scope.object.books);
    // },function(error) {
    // });
}
