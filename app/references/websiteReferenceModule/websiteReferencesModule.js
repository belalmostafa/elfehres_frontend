'use strict';

var website =   angular.module('elfehres.reference.website', ['ui.router', 'ui.bootstrap']);

website.config(function($stateProvider) {

    $stateProvider.state('reference.website', {
        url : '/references/{referenceId}/website',
        templateUrl : "references/websiteReferenceModule/websiteReferencesView.html",
        controller : "websiteReferencesController"
    });
});

website.controller('websiteReferencesController', singleReferenceDisplayFunction);

singleReferenceDisplayFunction.$inject=['$scope', '$state', '$uibModal','$stateParams'];

function singleReferenceDisplayFunction($scope, $state, $uibModal, $stateParams){
    console.log($scope.object);
    // $scope.object = {};
    // $scope.$state = $state;
    // getBooks.getBooksOfQuestionId({'questionId': $stateParams.referenceId}).then(function (replies){
    //     $scope.object.books  = replies;
    //     console.log("hello");
    //     console.log($scope.object.books);
    // },function(error) {
    // });
}
