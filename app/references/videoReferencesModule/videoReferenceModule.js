'use strict';

var videoModule = angular.module('elfehres.reference.videos', ['ui.router', 'ui.bootstrap']);

videoModule.config(function($stateProvider) {

    $stateProvider.state('reference.videos', {
        url : '/references/{referenceId}/videos',
        templateUrl : "references/videoReferencesModule/index.html",
        controller : "videoReferencesController"
    });
});

videoModule.controller('videoReferencesController', singleVideoReferenceDisplayFunction);

singleVideoReferenceDisplayFunction.$inject=['$scope', '$state', '$uibModal','$stateParams'];

function singleVideoReferenceDisplayFunction($scope, $state, $uibModal, $stateParams){

console.log($scope.object);
}
