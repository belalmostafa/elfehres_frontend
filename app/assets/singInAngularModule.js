
var signInModule = angular.module('signIn', ['ui.bootstrap','ngCookies']);

signInModule.constant('base_url', "http://localhost:8010/api/v2/");
signInModule.service('elfehres_httpSerivce', function($http, base_url , $q ){


    var post = function(path, params, headers=null){
        var deferred = $q.defer();

        $http({
            url: base_url+path,
            method: "POST",
            params: params

        }).then(successCallBack, errorCallback);
        function successCallBack(response){
            deferred.resolve(response.data);

        }
        function errorCallback(response){
            deferred.reject(response.data);
        }
        return deferred.promise;

    };

    var get = function(path, params, headers = null){

        var deferred = $q.defer();

        $http({
            url: base_url+path,
            method: "GET",
            params: params,
        }).then(successCallBack, errorCallback);
        function successCallBack(response){
                deferred.resolve(response.data);

        }
        function errorCallback(response){
            deferred.reject(response.data);
        }

        return deferred.promise;

    };

    return {
        post: post,
        get: get
    };

});

signInModule.service('submitSignInRequestService', function ($q, $http, base_url, elfehres_httpSerivce){

    //TODO finish the back end call
    var authenticate = function(params){

        var deferred = $q.defer();

        elfehres_httpSerivce.post('authenticate',params, false).then(function (result){
            deferred.resolve(result);
        },function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    return {
        authenticate : authenticate,
    }
});


signInModule.controller('SignInController',SignInControllerFunction);

SignInControllerFunction.$inject=['$scope', 'submitSignInRequestService','$cookies','$window'];

function SignInControllerFunction($scope, submitSignInRequestService, $cookies, $window){

    $scope.signIn = function (){
        var email = document.getElementById('email').value;
        var password = document.getElementById('password').value;
        var formData = {'email': email, 'password': password};
        submitSignInRequestService.authenticate(formData).then(function (responseData){
            $cookies.put('_tk', responseData['token']);
            $window.location.href = '/';
        },function(error) {
        });
    }
}
