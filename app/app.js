'use strict';

// Declare app level module which depends on views, and components
angular.module('elfehres', [
    'ui.router',
    'ngTagsInput',
    'ngAnimate',
    'ui.bootstrap',
    'elfehres.home',
    'elfehres.reference',
    'ngCookies',
    'elfehres.reference.books',
    'elfehres.reference.scp',
    'elfehres.reference.website',
    'elfehres.reference.videos',
    'elfehres.main'

]).config(['$urlRouterProvider', function($urlRouterProvider) {

  $urlRouterProvider.otherwise('/references');


}]).constant('hostname', "http://localhost:8000")
    .constant('base_url', "http://localhost:8010/api/v2/")

    .service('uuid4', function(){
      const pattern = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';

      const replacements = {
        x: '0123456789abcdef',
        y: '89ab'
      };

      const pattern_re =
          /[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}/;


      function getRandomCharacter (char_list) {
        const random_position = Math.floor(Math.random() * char_list.length);
        return char_list.charAt(random_position);
      }


      function getPatternCharacter (character) {
        return (character in replacements)
            ? getRandomCharacter(replacements[character])
            : character;
      }


      /**
       * Checks if provided `id` is valid UUID4.
       * @param {string} id
       * @returns {boolean}
       */
      function validate (id) {
        return pattern_re.test(id);
      }


      /**
       * Generates new valid UUID4 identifier.
       * @returns {string}
       */
      function generate () {
        return pattern
            .split('')
            .map(getPatternCharacter)
            .join('');
      }

      return {
        generate: generate,
        validate: validate
      }
    })


    .service ('getClientIpService' ,function($http , $q){

      var getClientIp = function(){
        console.log("inside get client ip func")
        var client_ip = '';

        var deferred = $q.defer();

        $http({
          url:"https://ipinfo.io",
          method: "GET",
        }).success(function(response, status, headers, config) {
          client_ip = response.ip;
          deferred.resolve(client_ip);

        }).error(function(response, status, headers, config) {
          deferred.reject(data);
        });

        return deferred.promise;
      }

      return {
        getClientIp : getClientIp
      }




    })

    .service('elfehres_httpSerivce', function($http, base_url , $q , validateTokenService, $cookies, hostname){


      var post = function(path, params, headers=null){
          params['token']   =   $cookies.get('_tk');
        var deferred = $q.defer();

        $http({
          url: base_url+path,
          method: "POST",
          params: params

        }).success(function(data, status, headers, config) {

          deferred.resolve(data);

        }).error(function(data, status, headers, config) {

          deferred.reject(data);
        });
        return deferred.promise;

      };

      var get = function(path, params, headers = null){

          //
          // validateTokenService.validateToken().then(function (response){
          //   console.log(response);
          // },function(error) {
          //           //window.location   = hostname + '/signin.html';
          //
          //
          // });

        var deferred = $q.defer();

        $http({
          url: base_url+path,
          method: "GET",
          params: params

        }).success(function(data, status, headers, config) {

          deferred.resolve(data);

        }).error(function(data, status, headers, config) {

          deferred.reject(data);
        });
        return deferred.promise;

      };

      return {
        post: post,
        get: get
      };

    })

    .service('validateTokenService', function ($http, base_url, $q, $cookies){

        var validateToken = function(){


            var deferred = $q.defer();

            $http({
                url: base_url+'validateToken',
                method: "POST",
                params: {'token':$cookies.get('_tk')}

            }).success(function(data, status, headers, config) {

                deferred.resolve(data);

            }).error(function(data, status, headers, config) {

                deferred.reject(data);
            });

            return deferred.promise;
        };

        return {
            validateToken : validateToken
        }
    });


