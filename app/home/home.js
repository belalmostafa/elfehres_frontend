'use strict';

var home = angular.module('elfehres.home', ['ui.router', 'ui.bootstrap']);

home.config(function($stateProvider) {

  $stateProvider.state('references', {

    url : '/references',
    templateUrl : "home/index.html",
    controller : "HomeCtrl"
  });
});

home.controller('HomeCtrl', homeControllerFunction);

homeControllerFunction.$inject=['$scope', 'getAllReferencesService', '$state', '$uibModal','$cookies'];

function homeControllerFunction($scope, getAllReferencesService, $state, $uibModal,$cookies){
  var vm = this;
  $scope.openCommentsModal = function(post) {
    var modalInstance = $uibModal.open({
      animation: true,
      component: 'commentModal',
      resolve: {
        curPost: function(){
          return post;
        }
      }

    });
  };

  $scope.object = {};
  getAllReferencesService.getAllReferencesForHomePage({'limit':20}).then(function (questions){
    questions.forEach(function (question) {
      var notes = createElement('div', 'notes', 'none');
      var quill = new Quill('#notes');
      quill.setContents(JSON.parse(question.questionNotes));
      question.questionNotes  = quill.getText();
    });

      $scope.object.references  = questions;
    console.log(questions)
  },function(error) {
    });



  function createElement (el, id, display){
    var div     = document.createElement(el);
        div.id  = id;
        div.style.display = display;
    document.body.appendChild(div);
    return div;
  }
}


