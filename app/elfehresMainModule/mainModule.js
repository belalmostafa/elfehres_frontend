'use strict';

var mainModule = angular.module('elfehres.main' , ['ui.router','ngCookies','ui.bootstrap']);

mainModule.controller('mainController', mainControllerFunction);

mainControllerFunction.$inject=['$scope','$cookies', '$uibModal', '$state'];

function mainControllerFunction($scope, $cookies, $uibModal){
    $scope.isLoggedIn = ($cookies.get('_tk') != null) ;
    $scope.requestReferenceModal = function(post) {
        var modalInstance = $uibModal.open({
            animation: true,
            component: 'requestReferencesModalComponent',
            backdrop: 'static'
        });

        $scope.cancel = function(){
            modalInstance.dismiss('cancel');
        };
    };
}